import { createApp } from 'vue'
import App from './App'
import components from '@/components/UI';
import './../node_modules/bulma/css/bulma.css';
import maincomponents from '@/components';
const app = createApp(App);

components.forEach(component => {
    app.component(component.name, component)
}),
maincomponents.forEach(component => {
    app.component(component.name, component)
}),

app.mount('#app')
