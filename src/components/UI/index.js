// import MyAdd from "@/components/UI/MyAdd";
import MyInput from "@/components/UI/MyInput";
import MyDialog from "@/components/UI/MyDialog";
import MyMap from "@/components/UI/MyMap";


export default [
//    MyAdd,
    MyInput,
    MyDialog,
    MyMap,
];
