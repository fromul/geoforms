import GeoCell from "@/components/GeoCell";
import GeoTable from "@/components/GeoTable";


export default [
    GeoCell,
    GeoTable,
];
